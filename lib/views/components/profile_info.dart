import 'package:flutter/material.dart';

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({super.key});

  @override
  Widget build(BuildContext context) {
    TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      leading: Image.asset('assets/images/png/avatar.png'),
      title: Text(
        'Sanbonani!',
        style: theme.headlineLarge,
      ),
      subtitle: Text(
        'Sanya Chauke',
        style: theme.headlineSmall,
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Image.asset('assets/images/png/notifications.png'),
          const SizedBox(width: 20),
          const Icon(Icons.more_vert, color: Colors.white),
        ],
      ),
    );
  }
}

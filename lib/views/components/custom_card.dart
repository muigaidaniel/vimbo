import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomCard extends StatelessWidget {
  const CustomCard(
      {super.key,
      this.color,
      this.title,
      this.subtitle,
      this.subAction,
      this.leafRequired,
      required this.action});
  final Color? color;
  final String? title;
  final String? subtitle;
  final String? subAction;
  final bool? leafRequired;
  final Widget action;

  @override
  Widget build(BuildContext context) {
    TextTheme theme = Theme.of(context).textTheme;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      decoration: BoxDecoration(
          color: color ?? const Color(0xFFD5F8E6),
          borderRadius: const BorderRadius.all(Radius.circular(15))),
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Visibility(
            visible: leafRequired ?? false,
            child: ClipRRect(
              borderRadius:
                  const BorderRadius.only(bottomRight: Radius.circular(15)),
              child: SvgPicture.asset(
                'assets/images/svg/leaf.svg',
                //width: 100, height: 100
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(15),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8, vertical: 2),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              )),
                          child: Text(subAction ?? '',
                              style: theme.bodySmall!.copyWith(fontSize: 11))),
                      const SizedBox(height: 10),
                      Text(title ?? '',
                          style: theme.bodyMedium!.copyWith(fontSize: 18)),
                    ],
                  ),
                  action
                ],
              ),
              const SizedBox(height: 10),
              Text(subtitle ?? '', style: theme.bodySmall)
            ]),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:vimbo/views/components/custom_card.dart';
import 'package:vimbo/views/components/suggestions.dart';

import '../components/profile_info.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    TextTheme theme = Theme.of(context).textTheme;
    Color primaryColor = Theme.of(context).primaryColor;
    return Scaffold(
      backgroundColor: primaryColor,
      body: ListView(
        shrinkWrap: true,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const ProfileInfo(),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Let’s meet you where you are',
                        style: theme.headlineMedium,
                      ),
                      const SizedBox(height: 10),
                      Text(
                          'Get started with our questionnaire, this will help us set you up for success and curate your experience. ',
                          style: theme.headlineSmall),
                    ],
                  ),
                ),
                Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.all(15),
                            backgroundColor: const Color(0xFF35998B)),
                        child: Text('Start Questionnaire',
                            style: theme.labelMedium)))
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20, bottom: 0),
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomCard(
                  subAction: 'Quick Start',
                  title: 'Vimbo in 1 min',
                  subtitle:
                      'No time for reading? No sweat! Take a listen to what Vimbo is all ablout and where you can start.',
                  action: ElevatedButton.icon(
                      onPressed: () {},
                      icon: Image.asset('assets/images/png/play.png',
                          color: primaryColor),
                      label: Text('Play',
                          style: theme.headlineSmall!
                              .copyWith(color: primaryColor)),
                      style: ElevatedButton.styleFrom(
                          foregroundColor: primaryColor,
                          backgroundColor: Colors.white)),
                ),
                Text(
                  'Need help, ask away',
                  style: theme.bodyMedium,
                ),
                const SizedBox(height: 10),
                TextField(
                  style: theme.bodySmall,
                  decoration: InputDecoration(
                    hintText: 'Ask us anything..',
                    hintStyle: theme.bodySmall,
                    prefixIcon: const Icon(Icons.search),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide.none),
                    filled: true,
                    fillColor: const Color(0xFFF3F4F6),
                  ),
                ),
                const SizedBox(height: 10),
                const Suggestions(),
                const SizedBox(height: 10),
                Text(
                  'Get to know us',
                  style: theme.bodyMedium,
                ),
                CustomCard(
                  leafRequired: true,
                  color: const Color(0xFFFFC273).withOpacity(0.7),
                  subAction: 'Take a Tour',
                  title: 'My Wellbeing Toolkit',
                  subtitle:
                      'Immerse yourself with knowledge to understand your journey,',
                  action: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                        foregroundColor: primaryColor,
                        backgroundColor: Colors.white),
                    child: Text('View',
                        style:
                            theme.headlineSmall!.copyWith(color: primaryColor)),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
